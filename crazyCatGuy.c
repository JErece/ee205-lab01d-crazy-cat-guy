///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - Crazy Cat Guy - EE 205 - Spr 2022
///
/// @file    crazyCatGuy.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o crazyCatGuy crazyCatGuy.c
///
/// Usage:  crazyCatGuy n
///   n:  Sum the digits from 1 to n
///
/// Result:
///   The sum of the digits from 1 to n is XX
///
/// Example:
///   $ ./crazyCatGuy 6
///   The sum of the digits from 1 to 6 is 21
///
/// @author  Justin Erece <erece8@hawaii.edu>
/// @date    16 January 2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) 
{
//Declare variables
int n = atoi(argv[1]);
int sum = 0;
int i = 1;

   //For all new additions 
   for(i = 1;i<=n;i++)
   {
      //Increment the new total by 1 for each addition
      sum += i;
   }
   
   //Display the sum of the digits to the screen
   printf("The sum of the digits from 1 to %d is %d\n", n, sum);

   return 0;

   
}
